from controller import Robot
import numpy as np
from deepbots.robots.controllers.robot_emitter_receiver_csv import \
    RobotEmitterReceiverCSV

def normalize_to_range(value, min, max, newMin, newMax):>>>>>>> Normalise function
    value = float(value)
    min = float(min)
    max = float(max)
    newMin = float(newMin)
    newMax = float(newMax)
    return (newMax - newMin) / (max - min) * (value - max) + newMax

class FindTargetRobot(RobotEmitterReceiverCSV):
    def __init__(self, n_rangefinders):				>>> Initialise
	
        super(FindTargetRobot, self).__init__()
        self.setup_rangefinders(n_rangefinders)
        self.setup_motors()

    def create_message(self):			 >> Creating message for sending the state 
        message = []
        for rangefinder in self.rangefinders:
            message.append(rangefinder.getValue())
        return message

    def use_message_data(self, message):   >> receiving message data for doing action 
        # Action 1 is gas
        gas = float(message[1])
        # Action 0 is turning
        wheel = float(message[0])
        
        gas *= 4
        wheel *= 2
        wheel = np.clip(wheel, -2, 2)

        self.motorSpeeds[0] = gas + wheel
        self.motorSpeeds[1] = gas - wheel
        
        self.motorSpeeds = np.clip(self.motorSpeeds, -4, 4)
        
        self.leftMotor.setVelocity(self.motorSpeeds[0])
        self.rightMotor.setVelocity(self.motorSpeeds[1])    
        
    def setup_rangefinders(self, n_rangefinders):   >>>  get the range 
        # Sensor setup
        self.n_rangefinders = n_rangefinders
        self.rangefinders = []
        self.psNames = ['ps' + str(i) for i in range(self.n_rangefinders)
                        ]         
        for i in range(self.n_rangefinders):
            self.rangefinders.append(
                self.robot.getDistanceSensor(self.psNames[i]))
            self.rangefinders[i].enable(self.timestep)

    def setup_motors(self):					 >> actuators
        # Motor setup
        self.leftMotor = self.robot.getMotor('left wheel motor')
        self.rightMotor = self.robot.getMotor('right wheel motor')
        self.leftMotor.setPosition(float('inf'))
        self.rightMotor.setPosition(float('inf'))
        self.leftMotor.setVelocity(0.0)
        self.rightMotor.setVelocity(0.0)
        
        self.motorSpeeds = [0.0, 0.0]

robot_controller = FindTargetRobot(8)
robot_controller.run()
